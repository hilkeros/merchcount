class AddMoneyToEvents < ActiveRecord::Migration
  def change
    add_column :events, :start_money, :string
    add_column :events, :end_money, :string
    add_column :events, :turnover, :string
  end
end
