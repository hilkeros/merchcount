class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.references :event, index: true, foreign_key: true
      t.references :product, index: true, foreign_key: true
      t.string :amount
      t.string :subtotal

      t.timestamps null: false
    end
  end
end
