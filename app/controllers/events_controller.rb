class EventsController < ApplicationController
  def new
  end

  def edit
  	@artist = Artist.find(params[:id])
  	@event = Event.find(params[:id])
  	@artist.products.build
    if current_user
      @user = current_user
    else
      @user = User.new
    end
  end

  def update
  	@event = Event.find(params[:id])
  	if @event.update_attributes(event_params)
  		respond_to do |format|
  			#format.html
  			format.js
  		end
  	end
  end

  private

  	def event_params
  		params.require(:event).permit(:location, :start_money, :end_money, :turnover)
  	end

end
