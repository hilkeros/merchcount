class ProductsController < ApplicationController
  def create
  	@product = Product.new(product_params)
  	if @product.save
      respond_to do |format|
  			format.html {redirect_to @product}
  			format.js
  		end
  	end
  end

  def update
  	@product = Product.find(params[:id])
  	if @product.update_attributes(product_params)
  		respond_to do |format|
  			format.html {redirect_to @product}
  			format.js
  		end
  	end
  end

  def index
  end

  def destroy
  	Product.find(params[:id]).destroy
  	respond_to do  |format|
  		format.html {redirect_to products_url}
  		format.js
  	end
  end

  private

  	def product_params
  		params.require(:product).permit(:name, :price, :artist_id)
  	end
end
