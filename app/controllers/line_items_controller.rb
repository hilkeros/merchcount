class LineItemsController < ApplicationController
	def create
		@line_item = LineItem.new(line_item_params)
		if @line_item.save
      respond_to do |format|
  			#format.html {redirect_to @product}
  			format.js
  		end
  	end
  end

  private
  def line_item_params
  	params.require(:line_item).permit(:event_id, :product_id, :amount, :subtotal)
  end
 end