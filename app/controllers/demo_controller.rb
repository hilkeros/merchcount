class DemoController < ApplicationController
  def home
  end

  def products
  end

  def start_money
  end

  def end_money
  end

  def selling
  end

  def event_saved
  end
end
