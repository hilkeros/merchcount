class ArtistsController < ApplicationController

	#before_action :authenticate_user!, :except => [:new, :create, :update]

	def index
		@artists = current_user.artists
	end

	def new
		@artist = Artist.new
		@artist.events.build
	end

	def create
		@artist = Artist.new(artist_params)
		if user_signed_in?
			@artist.user_id = current_user.id
		else
			@artist.user_id = guest_user.id
		end
		if @artist.save
			flash[:info] = "Artist saved"
			redirect_to edit_artist_event_path(@artist, @artist.events.last)
		else
			render 'new'
		end
	end

	def edit
		@artist = Artist.find(params[:id])
		@artist.products.build
	end

	def update
		@artist = Artist.find(params[:id])
		if @artist.update_attributes(artist_params)
			 if request.xhr?
          render :partial => 'products_form', :layout => false, :locals => { :artist => @artist }
        end
			respond_to do |format|
				format.html { redirect_to @artist }
				format.js
			end
		end
	end

	private

	def artist_params
		params.require(:artist).permit(:name, events_attributes: [ :id, :location ], products_attributes: [:id, :name, :price, :_destroy])
	end

end
