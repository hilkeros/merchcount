class Product < ActiveRecord::Base
  belongs_to :artist
  has_many :line_items

  validates :name, presence: true
end
