class Event < ActiveRecord::Base
  belongs_to :artist
  has_many :line_items, dependent: :destroy
  accepts_nested_attributes_for :line_items, :reject_if => lambda { |a| a[:amount].blank? }, :allow_destroy => true


  validates :location, presence: true
end
