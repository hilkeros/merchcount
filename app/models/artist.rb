class Artist < ActiveRecord::Base
  belongs_to :user
  has_many :events, dependent: :destroy
  has_many :products, dependent: :destroy
  accepts_nested_attributes_for :events, :reject_if => lambda { |a| a[:location].blank? }, :allow_destroy => true
  accepts_nested_attributes_for :products, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true

  validates :name, presence: true
end
