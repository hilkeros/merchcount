# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->

	eventId = $('#start-money').data("event-id")
	totalStartMoney = 0
	totalEndMoney = 0
	line_items = []
	turnover = 0

	#Show next screen after creating products
	$("#products-next-button").click ->
		console.log "next clicked"
		$("#products").addClass 'hidden'
		$("#start-money").removeClass 'hidden'
		$("#start-money").show

	#Count start money
	$(".number-input").blur -> 

		hundreds = $( "input[name='hundreds']" ).val()
		fifties = $( "input[name='fifties']" ).val()
		twenties = $( "input[name='twenties']" ).val()
		tens = $( "input[name='tens']" ).val()
		fives = $( "input[name='fives']" ).val()
		twos = $( "input[name='twos']" ).val()
		ones = $( "input[name='ones']" ).val()
		fiftycents = $( "input[name='fiftycents']" ).val()
		twentycents = $( "input[name='twentycents']" ).val()
		tencents = $( "input[name='tencents']" ).val()
		totalStartMoney = (hundreds * 100) + (fifties * 50) + (twenties *20) + (tens * 10) + (fives * 5) + (twos * 2) + (ones * 1) + (fiftycents * 0.5) + (twentycents * 0.2) + (tencents * 0.1)
		console.log totalStartMoney

		$( "#totalStartMoney" ).text(totalStartMoney)
		return

	$("#start-money-next-button").click ->
		console.log "next clicked, eventId is", eventId
		$.ajax
			url: '/events/'+eventId
			type: 'PATCH'
			dataType: 'json'
			data: {"event": {"start_money": totalStartMoney }}

		$("#start-money").addClass 'hidden'
		$("#selling").removeClass 'hidden'
		$("#selling").show

	#Push plus und minus button while selling
	increase_line_item_count = (selected_product)->
		line_items[selected_product] = 0 unless line_items[selected_product]
		line_items[selected_product] += 1
		console.log "The count is updated to", line_items[selected_product]
		
	decrease_line_item_count = (selected_product)->
		line_items[selected_product] = 0 unless line_items[selected_product]
		line_items[selected_product] -= 1 unless line_items[selected_product] == 0 
	
	update_line_item_count = (product_id) ->
		$("li[data-product-id=" + product_id + "]").find(".count-bubble").text(line_items[product_id])

	$(document).on 'click', '.plus', (event) ->
		selected_product = $(event.target).parents("li").data('product-id')
		increase_line_item_count selected_product
		update_line_item_count selected_product
		
	$(document).on 'click', '.min', (event) ->
		selected_product = $(event.target).parents("li").data('product-id')
		decrease_line_item_count selected_product
		update_line_item_count selected_product

	#Save line items for this event and go to the next screen
	$("#after-sales-next-button").click ->
		console.log "next after sales clicked, eventId is", eventId		
		$(".iphone-list-item").each ->
			productId = $(this).data("product-id")
			amount = $(this).find(".count-bubble").text()
			subtotal = amount * $(this).data("price-id")
			turnover = turnover + subtotal
			console.log productId, amount, subtotal
			$.ajax
				url: '/line_items'
				type: 'POST'
				dataType: 'json'
				data: {"line_item": {"event_id": eventId, "product_id": productId, "amount": amount, "subtotal": subtotal}}

			$("#selling").addClass 'hidden'
			$("#end-money").removeClass 'hidden'
			$("#end-money").show

	#Count end money
	$(".enumber-input").blur -> 

		ehundreds = $( "input[name='ehundreds']" ).val()
		efifties = $( "input[name='efifties']" ).val()
		etwenties = $( "input[name='etwenties']" ).val()
		etens = $( "input[name='etens']" ).val()
		efives = $( "input[name='efives']" ).val()
		etwos = $( "input[name='etwos']" ).val()
		eones = $( "input[name='eones']" ).val()
		efiftycents = $( "input[name='efiftycents']" ).val()
		etwentycents = $( "input[name='etwentycents']" ).val()
		etencents = $( "input[name='etencents']" ).val()
		totalEndMoney = (ehundreds * 100) + (efifties * 50) + (etwenties *20) + (etens * 10) + (efives * 5) + (etwos * 2) + (eones * 1) + (efiftycents * 0.5) + (etwentycents * 0.2) + (etencents * 0.1)
		console.log totalEndMoney

		$( "#totalEndMoney" ).text(totalEndMoney)
		return

	$("#end-money-next-button").click ->
		console.log "next clicked, eventId is", eventId
		$.ajax
			url: '/events/'+eventId
			type: 'PATCH'
			dataType: 'json'
			data: {"event": {"end_money": totalEndMoney, "turnover": turnover }}

		$("#end-money").addClass 'hidden'
		$("#event-saved").removeClass 'hidden'
		$("#turnover").text(turnover)
		$("#event-saved").show
