require 'test_helper'

class DemoControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get products" do
    get :products
    assert_response :success
  end

  test "should get start_money" do
    get :start_money
    assert_response :success
  end

  test "should get end_money" do
    get :end_money
    assert_response :success
  end

  test "should get selling" do
    get :selling
    assert_response :success
  end

  test "should get event_saved" do
    get :event_saved
    assert_response :success
  end

end
